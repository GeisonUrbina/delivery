export interface LinkList{
    icon:string;
    name:string;
    redirectTo:string;
}

export interface DeliveryPerson{
    id:string;
    delivery_person_email:string;
    delivery_person_name:string;
    delivery_person_lastName:string;
    picture:string;
    delivery_person_identificationType:string;
    delivery_person_identificationNumber:string;
    delivery_person_role:string;
    delivery_person_phone:string;
    player_id:string;
    phone_code:string;
    phone_validated:string;
    validation_method:string;
    password:string;
    delivery_person_status:string;
    total_viajes:string;
}

export interface Order{
    codigo:string;
    montoTotal:string;
    status:string;
    forma_pago:string;
    carrito:string;
    created_at:string;
    latitud:any;
    longitud:any;
    delivery_person_name:string;
    delivery_person_lastName:string;
    delivery_person_phone:string;
    client_id:string;
    client_name:string;
    client_lastName:string;
    client_phone:string;
}

export interface ProductList{
    product_code:string;
    product_name:string;
    product_description:string;
    product_cost:string;
    product_com_profit:string;
    product_quantity:number;
    product_status:string;
    product_image:string;
    made_in:string;
    is_offer:string;
    monto:string;
    montoAporte:string;
}

export interface Cart{
    cliente:string;
    codigo:string;
    cantidad:string;
}