import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { LinkList } from '../../interfaces/interfaces';
import { LocalDataService } from '../../services/local-data.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss'],
})
export class SideMenuComponent implements OnInit {
  linkList: Observable<LinkList[]>;
  showDefault:boolean = true;
  constructor(private dataService:DataService,public localDataService:LocalDataService) {
    if(this.localDataService.user.picture !== undefined && this.localDataService.user.picture !== ""){
      this.showDefault = false;
    }
   }

  ngOnInit() {
    this.linkList = this.dataService.getSlideMenuOptions();
  }
}
