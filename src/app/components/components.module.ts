import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { SecondaryComponent } from './secondary/secondary.component';
import { SideMenuComponent } from './side-menu/side-menu.component';




@NgModule({
  declarations: [
    HeaderComponent,
    SecondaryComponent,
    SideMenuComponent],
  exports:[
    HeaderComponent,
    SecondaryComponent,
    SideMenuComponent],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule
  ]
})
export class ComponentsModule { }
