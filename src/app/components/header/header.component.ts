import { Component, OnInit, Input } from '@angular/core';
import { LocalDataService } from '../../services/local-data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  @Input() title:string;
  @Input() backText:string;
  @Input() backTextEnabled:boolean;
  @Input() message:boolean;
  @Input() menuToggleEnabled:boolean;

  constructor(public localDataService:LocalDataService) {}

  ngOnInit() {}

}
