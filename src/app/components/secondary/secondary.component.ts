import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-secondary',
  templateUrl: './secondary.component.html',
  styleUrls: ['./secondary.component.scss'],
})
export class SecondaryComponent implements OnInit {
  @Input() title:string;
  @Input() backText:string;
  constructor() { }

  ngOnInit() {}

}
