import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Order, ProductList } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  //urlServer : string = "https://www.urbidcr.com/";
  urlServer : string = "http://192.168.0.18:8000/";
  constructor(private http:HttpClient) { }


  getCart(cart_products){
    let cart = new FormData();
    cart.append('cart_products',cart_products);
    return this.http.post<ProductList[]>(this.urlServer + "administration/cart/cart-products",cart);
  }


  isSubscribed(loginType,deliveryPersonPhone){
    let deliveryPerson = new FormData();
    deliveryPerson.append('tipo_login',loginType);
    deliveryPerson.append('client_phone',deliveryPersonPhone);
    return this.http.post<any>(this.urlServer + "app/validate/deliveryPerson",deliveryPerson);
  }

  getSmsToken(phoneNumber:string){
    let telefono = new FormData();
    telefono.append('number',phoneNumber);
    return this.http.post<any>(this.urlServer + "app/phoneCode",telefono);
  }

  deliveryOrderList(deliveryPersonId){
    let orders = new FormData();
    orders.append('user_id',deliveryPersonId);
    return this.http.post<Order[]>(this.urlServer + "administration/order/deliveryOrderList",orders);
  }

  finishOrder(code,clientId,points,cart){
    let order = new FormData();
    order.append('codigo',code);
    order.append('cliente',clientId);
    order.append('puntos',points);
    order.append('carrito',cart);
    return this.http.post<any>(this.urlServer + "app/finish/order",order);
  }


  cancelOrder(code,clientId){
    let orderForm = new FormData();
    orderForm.append('orderCode',code);
    orderForm.append('id_cliente',clientId);
    return this.http.post<any>(this.urlServer + "administration/order/cancel",orderForm);
  }

  notifyArrival(clientId){
    let orderForm = new FormData();
    orderForm.append('id_cliente',clientId);
    return this.http.post<any>(this.urlServer + "administration/order/notify/arrival",orderForm);
  }

  updateDevice(clientId,device){
    let client = new FormData();
    client.append('delivery_person_id',clientId);
    client.append('delivery_person_device',device);
    return this.http.post(this.urlServer + "app/update/delivery/device",client);
  }

}
