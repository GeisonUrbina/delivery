import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController,LoadingController  } from '@ionic/angular';
import { DeliveryPerson} from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class LocalDataService {

  user: DeliveryPerson = <DeliveryPerson>{};
  loader:any;
  constructor(private toastCtrl:ToastController,public loadingCtrl: LoadingController,
              private storage:Storage) {
                this.uploadUser();
              }

  async uploadUser(){
    this.user = await this.storage.get('user');
  }

  saveUserData(user){
    this.user = user;
    this.storage.set('user',user);
  }

  async presentToast(message,color) {
    const toast = await this.toastCtrl.create({
      message: message,
      duration: 2000,
      color
    });
    toast.present();
  }

  async presentLoadingWithOptions() {
    this.loader = await this.loadingCtrl.create({
      spinner: 'bubbles',
      mode: 'ios',
      message: 'Cargando',
      translucent: true,
      backdropDismiss: true
    });
    await this.loader.present();
  }

  dismissLoader(){
    this.loader.dismiss();
  }

  async logout(){
    await this.storage.remove('user');
  }
}
