import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Injectable({
  providedIn: 'root'
})
export class PushNotificationService {
  userId:string;
  constructor(private oneSignal: OneSignal) { }


  initConfig(){
    this.oneSignal.startInit('d0021d32-28bc-4a57-8b89-bb4448e6f5ac', '772038011181');
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    this.oneSignal.handleNotificationReceived().subscribe((noti) => {
    });
    this.oneSignal.handleNotificationOpened().subscribe((noti) => {
    });

    this.oneSignal.getIds().then(id =>{
      this.userId = id.userId;
    }).catch(() => {
      this.userId = "NOT_FOUND";
    });

    this.oneSignal.endInit();
  }
}
