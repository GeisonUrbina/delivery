import { Injectable } from '@angular/core';
import { LinkList } from '../interfaces/interfaces';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) { }

  getSlideMenuOptions(){
    return this.http.get<LinkList[]>('/assets/data/slide-menu.json')
  }

  enumStatus(state){
    switch(state){
      case "PC":
        return "Procesando compra";
      case "PE":
        return "Pendiente entrega";
      case "CE":
        return "Compra entregada";
      case "CC":
        return "Compra cancelada";
      case "PACKED":
        return "Compra procesada"
      default:
        return "Estado invalido";
    }
  }

  enumPaymentType(paymentType){
    switch(paymentType){
      case "E":
        return "Efectivo";
      case "P":
        return "Puntos URBID";
      case "T":
        return "Tarjeta";
      case "S":
        return "Sinpe Movil";
      default:
        return "Forma de pago inválido";
    }
  }
}
