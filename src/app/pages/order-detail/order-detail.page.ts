import { Component, OnInit } from '@angular/core';
import { Router,NavigationExtras } from '@angular/router';
import { Order, ProductList } from '../../interfaces/interfaces';
import { ApiService } from '../../services/api.service';
import { LocalDataService } from '../../services/local-data.service';
import { AlertController, NavController } from '@ionic/angular';
import { SocialSharing} from '@ionic-native/social-sharing/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-order-detail',
  templateUrl: './order-detail.page.html',
  styleUrls: ['./order-detail.page.scss'],
})
export class OrderDetailPage implements OnInit {

  order : Order = <Order>{};
  productList:ProductList[]=[];
  constructor(private router:Router,private apiService:ApiService,
    private localDataService:LocalDataService,private alertCtrl:AlertController,
    private navCtrl:NavController,private socialSharing:SocialSharing,private callNumber:CallNumber) {
    if(this.router.getCurrentNavigation().extras.state){
      this.order = this.router.getCurrentNavigation().extras.state.order;
      this.loadCart();
    }
   }

  ngOnInit() {
  }

  loadCart(){
    this.localDataService.presentLoadingWithOptions();
    this.apiService.getCart(this.order.carrito).subscribe(resp=>{
      this.productList = resp;
      this.localDataService.dismissLoader();
    },()=>this.localDataService.dismissLoader());
  }

  makeACall(){
    this.callNumber.callNumber(this.order.client_phone,true);
  }

  sendMessage(){
    this.socialSharing.shareViaWhatsAppToReceiver('+506 '+this.order.client_phone,'Buenas '+this.order.client_name+' '+this.order.client_lastName+' mi nombre es: '+
    this.localDataService.user.delivery_person_name+
                                        ' '+this.localDataService.user.delivery_person_lastName+
                                        ' repartidor asignado de la orden de compra '+this.order.codigo,null,null);

  }

  deliveryRoute(){
    let extras :  NavigationExtras = {
      state : {
        lat : this.order.latitud,
        lng : this.order.longitud
      }
    }
    this.router.navigate(['/location'],extras);
  }

  notifyArrival(clientId){
    this.localDataService.presentLoadingWithOptions();
    this.apiService.notifyArrival(clientId).subscribe(()=>{
      this.localDataService.dismissLoader();
    },()=>{
      this.localDataService.dismissLoader();
    });
  }

  finishOrder(){
    let extras : NavigationExtras = {
      state: {
        order:this.order
      }
    }
    this.router.navigate(['/finish-delivery'],extras);
  }

  async presentAlertConfirm(){
    const alert = await this.alertCtrl.create({
      header: 'Confirmar cancelación',
      message: 'Desea eliminar la orden de compra',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }, {
          text: 'Aceptar',
          handler: () => {
            this.localDataService.presentLoadingWithOptions();
            this.apiService.cancelOrder(this.order.codigo,this.order.client_id).subscribe(()=>{
              this.localDataService.dismissLoader();
              this.navCtrl.navigateRoot("/menu");

            },()=>this.localDataService.dismissLoader());
          }
        }
      ]
    });

    await alert.present();
  }
}
