import { Component, OnInit } from '@angular/core';
import { LocalDataService } from '../../services/local-data.service';
import { AlertController, NavController } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.page.html',
  styleUrls: ['./configuration.page.scss'],
})
export class ConfigurationPage implements OnInit {

  constructor(private localDataService:LocalDataService,
    public alertController: AlertController,
    private apiService:ApiService,private navCtrl:NavController) { }

  ngOnInit() {
  }

  logout(){
    const resul = this.localDataService.logout();
    this.navCtrl.navigateRoot('/home');
  }
  
}
