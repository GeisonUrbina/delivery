import { Component, OnInit } from '@angular/core';
import { Order } from '../../interfaces/interfaces';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ApiService } from '../../services/api.service';
import { LocalDataService } from '../../services/local-data.service';

@Component({
  selector: 'app-finish-delivery',
  templateUrl: './finish-delivery.page.html',
  styleUrls: ['./finish-delivery.page.scss'],
})
export class FinishDeliveryPage implements OnInit {
  order : Order = <Order>{};
  points : number = 0;
  constructor(private router:Router,private navCtrl:NavController,private apiService:ApiService,
    private localDataService:LocalDataService) { 
    if(this.router.getCurrentNavigation().extras.state){
      this.order = this.router.getCurrentNavigation().extras.state.order;
    }
  }

  ngOnInit() {
  }


  finishDelivery(){
    this.localDataService.presentLoadingWithOptions();
    this.apiService.finishOrder(this.order.codigo,this.order.client_id,this.points,this.order.carrito).subscribe(()=>{
      this.localDataService.dismissLoader();
      this.navCtrl.navigateRoot('/menu');
    },()=>{
      this.localDataService.dismissLoader();
    }); 
  }

}
