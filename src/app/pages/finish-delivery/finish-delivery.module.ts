import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FinishDeliveryPageRoutingModule } from './finish-delivery-routing.module';

import { FinishDeliveryPage } from './finish-delivery.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FinishDeliveryPageRoutingModule,
    ComponentsModule
  ],
  declarations: [FinishDeliveryPage]
})
export class FinishDeliveryPageModule {}
