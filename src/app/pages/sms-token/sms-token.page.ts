import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { FormBuilder,Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { LocalDataService } from '../../services/local-data.service';
import { NavController } from '@ionic/angular';
import { PushNotificationService } from '../../services/push-notification.service';

@Component({
  selector: 'app-sms-token',
  templateUrl: './sms-token.page.html',
  styleUrls: ['./sms-token.page.scss'],
})
export class SmsTokenPage implements OnInit {
  phone_code:string;
  data:any;
  phoneNumber:any;
  status:string;
  cantidadIntentos:number = 3;
  mensajeIntentos:string = "";

  smsTokenForm = this.formBuilder.group(
    {
      code: ['',[Validators.required,Validators.pattern("^[0-9]{6}$")]]
    }
  );

  constructor(private router:Router,private apiService:ApiService,
    private formBuilder:FormBuilder,private localDataService:LocalDataService,
    private navCtrl:NavController,private pushNotificationService:PushNotificationService) {
      if(this.router.getCurrentNavigation().extras.state){
        this.status = this.router.getCurrentNavigation().extras.state.status;
        this.data = this.router.getCurrentNavigation().extras.state.user;
        this.phoneNumber = this.router.getCurrentNavigation().extras.state.phoneNumber
        this.getSmsToken();
      }else{
        this.navCtrl.navigateRoot('/home');
      }
     }

  ngOnInit() {
  }


  public submit(){
    if(this.smsTokenForm.value.code == this.phone_code){
      if(this.status === "CREADO"){
        this.localDataService.saveUserData(this.data);
        this.apiService.updateDevice(this.data.id,this.pushNotificationService.userId).subscribe(()=>{
          this.navCtrl.navigateRoot(["/menu"]);
        },errr => console.log(errr));
      }else{
        this.navCtrl.navigateRoot('/home');
      }
    }else{
      this.mostrarMensajeIntentos();
    }
  }

  getSmsToken(){
    this.apiService.getSmsToken(this.phoneNumber).subscribe(resp => {
      this.phone_code = resp.token_sms;
      console.log("Token:",this.phone_code);
    });
  }

  validateCode(value: any): void{
    this.navCtrl.navigateRoot('/home');
  }

  mostrarMensajeIntentos(){
    if(this.cantidadIntentos === 0){
      this.navCtrl.navigateRoot('/home');
    }else{
      this.mensajeIntentos = "Usted tiene " + this.cantidadIntentos + " intento restante.";
      this.cantidadIntentos -= 1;
    }
  }

}
