import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { MenuController, NavController } from '@ionic/angular';
import { LocalDataService } from '../../services/local-data.service';
import { DataService } from '../../services/data.service';
import { Order } from '../../interfaces/interfaces';
import { Router,NavigationExtras } from '@angular/router';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  myOrders : Order[] = [];
  isEmpty:boolean = false;
  constructor(public menuCtrl:MenuController,private apiService:ApiService,private localDataService:LocalDataService,
              private dataService:DataService,private router:Router) {
                this.menuCtrl.enable(true,"mainMenu");
              }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.load();
  }

  load(){
    this.isEmpty = false;
    this.localDataService.presentLoadingWithOptions();
    this.localDataService.uploadUser().then(()=>{
      this.apiService.deliveryOrderList(this.localDataService.user.id).subscribe(resp => {
        this.myOrders = [];
        for(let i=0;i<resp.length;i++){
          resp[i].forma_pago = this.dataService.enumPaymentType(resp[i].forma_pago);
          resp[i].status = this.dataService.enumStatus(resp[i].status);
          this.myOrders.push(resp[i]);
        }
        if(resp.length === 0) this.isEmpty = true;

        this.localDataService.dismissLoader();
      },()=>this.localDataService.dismissLoader());
    });
  }

  detail(order:any){
    let navigationExtras : NavigationExtras = {
      state:{
        order: order
      }
    }
    this.router.navigate(['order-detail'],navigationExtras);
  }

}
