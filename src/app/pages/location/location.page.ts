import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Router } from '@angular/router';

declare var google;
@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})

export class LocationPage implements OnInit {
  @ViewChild('map_canvas',{static: false}) mapNativeElement: ElementRef;
  latitud:any;
  longitud:any;
  currentLat:any;
  currentLng:any;
  directionsService:any;
  directionsDisplay:any;
  constructor(private geolocation:Geolocation,private router:Router) {
    if(this.router.getCurrentNavigation().extras.state){
      this.latitud = this.router.getCurrentNavigation().extras.state.lat;
      this.longitud = this.router.getCurrentNavigation().extras.state.lng;
      this.geolocation.getCurrentPosition().then((location) => {
        this.currentLat = location.coords.latitude;
        this.currentLng = location.coords.longitude;
        this.loadMap();
      }).catch();
    }

  }

  ngOnInit() {
  }

  async loadMap(){
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;
    
    const nMap = new google.maps.Map(this.mapNativeElement.nativeElement,{
      zoom: 18,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: {
        lat: this.currentLat,
        lng: this.currentLng
      }
    }); 

    this.directionsDisplay.setMap(nMap);
    this.directionsService.route({
      origin:new google.maps.LatLng(this.currentLat,this.currentLng),
      destination: new google.maps.LatLng(this.latitud,this.longitud),
      travelMode: 'DRIVING',
      drivingOptions: {
        departureTime: new Date(Date.now()),
        trafficModel: 'optimistic'
      }
    },(response , status) => {
      if(status === 'OK'){
        this.directionsDisplay.setDirections(response);
      }else{
        console.log(status);
      }
    });

  }

}
