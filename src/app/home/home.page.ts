import { Component } from '@angular/core';
import { MenuController, IonCheckbox, NavController } from '@ionic/angular';
import { LocalDataService } from '../services/local-data.service';
import { Router,NavigationExtras } from '@angular/router';
import { ApiService } from '../services/api.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  phoneNumber:string;
  isLoaded:boolean=false;
  termsConditions:IonCheckbox = <IonCheckbox>{};
  constructor(private localDataService:LocalDataService,public menuCtrl:MenuController,private router:Router
    ,private apiService:ApiService,private navCtrl:NavController,
    public alertController: AlertController) {
      this.menuCtrl.enable(false,"mainMenu");
    }


    ionViewWillEnter(){
      this.localDataService.uploadUser().then(() =>{
        if(this.localDataService.user){
          this.navCtrl.navigateRoot('/menu');
        }else{
          this.termsConditions.checked = true;
          this.isLoaded=true;
        }
      });
    }
  


    getLogin(phoneNumber:string,loginType:string){
      if(phoneNumber === '' || phoneNumber === undefined){
        this.localDataService.presentToast("Número de teléfono vacío o inválido","danger");
      }else{
        this.localDataService.presentLoadingWithOptions();
        this.apiService.isSubscribed("T",phoneNumber).subscribe(resp => {
          let navigationExtras : NavigationExtras = {
            state:{
              status: resp.status_response,
              user: resp.user,
              phoneNumber
            }
          }
          this.localDataService.dismissLoader();
          this.router.navigate(['sms-token'],navigationExtras);
          
        }, () =>{
          this.localDataService.dismissLoader();
        });
      }
    }

}
